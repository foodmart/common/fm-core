package fm.common.core.api.mapper;

import fm.common.core.exception.ManyMappersFoundException;
import fm.common.core.exception.MapperNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MapperProviderImpl implements MapperProvider {

    private final ApplicationContext applicationContext;

    @Override
    public <T1, T2, T3, T4, T5, R> R map(@NonNull T1 t1, @NonNull T2 t2, @NonNull T3 t3, @NonNull T4 t4, @NonNull T5 t5, @NonNull Class<R> r) {
        String beanName = getBeanName(buildResolvableType(Mapper5.class, getType(t1), getType(t2), getType(t3), getType(t4), getType(t5), getReturnType(r)));
        Mapper5<T1, T2, T3, T4, T5, R> mapper = (Mapper5<T1, T2, T3, T4, T5, R>) applicationContext.getBean(beanName);
        return mapper.map(update(t1), update(t2), update(t3), update(t4), update(t5));
    }


    @Override
    public <T1, T2, T3, T4, R> R map(@NonNull T1 t1, @NonNull T2 t2, @NonNull T3 t3, @NonNull T4 t4, @NonNull Class<R> r) {
        String beanName = getBeanName(buildResolvableType(Mapper4.class, getType(t1), getType(t2), getType(t3), getType(t4), getReturnType(r)));
        Mapper4<T1, T2, T3, T4, R> mapper = (Mapper4<T1, T2, T3, T4, R>) applicationContext.getBean(beanName);
        return mapper.map(update(t1), update(t2), update(t3), update(t4));
    }

    @Override
    public <T1, T2, T3, R> R map(@NonNull T1 t1, @NonNull T2 t2, @NonNull T3 t3, @NonNull Class<R> r) {
        String beanName = getBeanName(buildResolvableType(Mapper3.class, getType(t1), getType(t2), getType(t3), getReturnType(r)));
        Mapper3<T1, T2, T3, R> mapper = (Mapper3<T1, T2, T3, R>) applicationContext.getBean(beanName);
        return mapper.map(update(t1), update(t2), update(t3));
    }

    @Override
    public <T1, T2, R> R map(@NonNull T1 t1, @NonNull T2 t2, @NonNull Class<R> r) {
        String beanName = getBeanName(buildResolvableType(Mapper2.class, getType(t1), getType(t2), getReturnType(r)));
        Mapper2<T1, T2, R> mapper = (Mapper2<T1, T2, R>) applicationContext.getBean(beanName);
        return mapper.map(update(t1), update(t2));
    }

    @Override
    public <T, R> R map(@NonNull T t, Class<R> r) {
        String beanName = getBeanName(buildResolvableType(Mapper.class, getType(t), getReturnType(r)));
        Mapper<T, R> mapper = (Mapper<T, R>) applicationContext.getBean(beanName);
        return mapper.map(update(t));
    }

    private String getBeanName(ResolvableType resolvableType) {
        String[] beanNames = applicationContext.getBeanNamesForType(resolvableType);
        if (beanNames.length > 1) {
            throw new ManyMappersFoundException();
        }
        if (beanNames.length == 0) {
            throw new MapperNotFoundException();
        }
        return beanNames[0];
    }

    private ResolvableType buildResolvableType(Class<?> aClass, ResolvableType... resolvableTypes) {
        return ResolvableType.forClassWithGenerics(aClass, resolvableTypes);
    }

    private <R> ResolvableType getReturnType(Class<R> r) {
        return ResolvableType.forClass(r);
    }

    private <T> ResolvableType getType(T t) {
        if (t instanceof Class<?>) {
            try {
                return ResolvableType.forClass(Class.forName(((Class<?>) t).getName()));
            } catch (ClassNotFoundException e) {
                throw new MapperNotFoundException(e.getMessage());
            }
        }
        return (t instanceof List)
                ? ResolvableType.forClassWithGenerics(List.class, ResolvableType.forInstance(t).getGeneric(0))
                : ResolvableType.forInstance(t);
    }

    private static <T> T update(T t) {
        if (t instanceof Class<?>) {
            t = null;
        }
        return t;
    }


}
