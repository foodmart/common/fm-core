package fm.common.core.api.mapper;

public interface MapperProvider {
    <T1, T2, T3, T4, T5, R> R map(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, Class<R> r);

    <T1, T2, T3, T4, R> R map(T1 t1, T2 t2, T3 t3, T4 t4, Class<R> r);

    <T1, T2, T3, R> R map(T1 t1, T2 t2, T3 t3, Class<R> r);

    <T1, T2, R> R map(T1 t1, T2 t2, Class<R> r);

    <T, R> R map(T t, Class<R> r);
}
