package fm.common.core.api.mapper;

public interface Mapper<T, R> {

    R map(T t);
}
