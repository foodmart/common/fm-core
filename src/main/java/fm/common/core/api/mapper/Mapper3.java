package fm.common.core.api.mapper;

public interface Mapper3<T1, T2, T3, R> {

    R map(T1 t1, T2 t2, T3 t3);
}
