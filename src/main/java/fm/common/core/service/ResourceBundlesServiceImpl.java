package fm.common.core.service;

import fm.common.core.model.header.CommonContext;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ResourceBundlesServiceImpl implements ResourceBundlesService {

    private final MessageSource messageSource;


    private final CommonContext commonContext;

    @Override
    public <T, R extends T> String getExceptionMessage(Class<T> clazz, R r) {
        return messageSource.getMessage(r.getClass().getSimpleName(), null, commonContext.getLocale());
    }
}
