package fm.common.core.service;

public interface ResourceBundlesService {

    <T, R extends T> String getExceptionMessage(Class<T> clazz, R r);
}
