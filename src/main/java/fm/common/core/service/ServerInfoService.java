package fm.common.core.service;

public interface ServerInfoService {
    String getServerUrl();
}
