package fm.common.core.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

@Service
public class ServerInfoServiceImpl implements ServerInfoService {


    @Value("${server.port:8000}")
    private int port;

    @Value("${server.ssl.enabled:false}")
    private boolean sslEnabled;

    public String getIpAddress() {
        try {
            final Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                final NetworkInterface networkInterface = networkInterfaces.nextElement();
                final Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()) {
                    final InetAddress inetAddress = inetAddresses.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress.isSiteLocalAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
            return null;
        } catch (SocketException e) {
            throw new RuntimeException("No Suitable ip found");
        }
    }

    @Override
    public String getServerUrl() {
        String protocol = sslEnabled ? "https" : "http";
        String ip = getIpAddress();
        return String.format("%s://%s:%d", protocol, ip, port);
    }
}
