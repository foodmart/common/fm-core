package fm.common.core.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParamsConstants {

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Common {
        public static final  String REQUEST_ID = "Request-Id";
        public static final  String PAGE = "page";
        public static final  String SIZE = "size";
        public static final  String TIME_ZONE = "Time-Zone";
        public static final  String LANGUAGE = "Lang";
        public static final  String COUNTRY = "Country";
    }


}
