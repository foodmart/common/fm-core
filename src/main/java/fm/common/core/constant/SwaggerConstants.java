package fm.common.core.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SwaggerConstants {


    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Descriptions {

        public static final String COUNTRY_DESCRIPTION = "Defines the country of the request";
        public static final String LANGUAGE_DESCRIPTION = "Defines the language of the request";
        public static final String TIME_ZONE_DESCRIPTION = "Defines the time zone of the operation which required timing";
        public static final String REQUEST_ID_DESCRIPTION = "Defines a unique identifier for the request through services";
        public static final String PAGE_DESCRIPTION = "Defines a page number of the requested query";
        public static final String SIZE_DESCRIPTION = "Defines a page size of the requested query";
    }
}
