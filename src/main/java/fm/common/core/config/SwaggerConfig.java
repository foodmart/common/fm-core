package fm.common.core.config;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.annoation.Param;
import fm.common.core.service.ServerInfoService;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.media.IntegerSchema;
import io.swagger.v3.oas.models.media.StringSchema;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.servers.Server;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

import static fm.common.core.util.UUIDUtil.generateTimeBasedUUID;

@Configuration
@RequiredArgsConstructor
public class SwaggerConfig {

    private final CoreConfig coreConfig;

    private final ServerInfoService serverInfoService;

    @Bean
    public OpenAPI openAPI() {
        final CoreConfig.Swagger swagger = coreConfig.getSwagger();

        return new OpenAPI()
                .info(new Info().title(coreConfig.getSpringAppName())
                        .version(swagger.getVersion())
                        .description(swagger.getDescription())
                        .contact(new Contact().name(swagger.getContact().getName())
                                .email(swagger.getContact().getEmail())
                                .url(swagger.getContact().getUrl()))
                        .license(new License()
                                .name(swagger.getLicense().getName())
                                .url(swagger.getLicense().getUrl())))
                .servers(List.of(
                        new Server().url(coreConfig.getGatewayUrl() + coreConfig.getContextPath())
                                .description("Gateway"),
                        new Server().url(serverInfoService.getServerUrl() + coreConfig.getContextPath())
                                .description("Direct")));
    }

    @Bean
    public OperationCustomizer customize() {
        return (operation, handlerMethod) -> {
            Optional.ofNullable(handlerMethod.getMethodAnnotation(CommonParams.class))
                    .ifPresent(commonParams -> {
                        final Param country = commonParams.country();
                        final Param language = commonParams.language();
                        final Param requestId = commonParams.requestId();
                        final Param timeZone = commonParams.timeZone();
                        operation.addParametersItem(new Parameter().in(country.type().getValue())
                                        .required(country.required())
                                        .description(country.description())
                                        .name(country.name())
                                        .schema(new StringSchema()._default(coreConfig.getCountry())))
                                .addParametersItem(new Parameter().in(language.type().getValue())
                                        .required(language.required())
                                        .description(language.description())
                                        .name(language.name())
                                        .schema(new StringSchema()._default(coreConfig.getLanguage())))
                                .addParametersItem(new Parameter().in(timeZone.type().getValue())
                                        .required(timeZone.required())
                                        .description(timeZone.description())
                                        .name(timeZone.name())
                                        .schema(new StringSchema()._default(coreConfig.getTimeZone())))
                                .addParametersItem(new Parameter().in(requestId.type().getValue())
                                        .required(requestId.required())
                                        .description(requestId.description())
                                        .name(requestId.name())
                                        .schema(new StringSchema().example(generateTimeBasedUUID())));
                    });
            Optional.ofNullable(handlerMethod.getMethodAnnotation(Pageable.class))
                    .ifPresent(pageable -> {
                        final Param page = pageable.page();
                        final Param size = pageable.size();
                        operation.addParametersItem(new Parameter().in(page.type().getValue())
                                        .required(page.required())
                                        .name(page.name())
                                        .schema(new IntegerSchema()._default(0)
                                                .example(0)))
                                .addParametersItem(new Parameter().in(size.type().getValue())
                                        .required(size.required())
                                        .name(size.name())
                                        .schema(new IntegerSchema()._default(Integer.MAX_VALUE)
                                                .example(10))
                                );
                    });
            return operation;
        };
    }

}
