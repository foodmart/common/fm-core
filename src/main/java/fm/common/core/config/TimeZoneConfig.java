package fm.common.core.config;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.TimeZone;

@Configuration
@DependsOn("coreConfig")
@RequiredArgsConstructor
public class TimeZoneConfig {

    private final CoreConfig coreConfig;

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone(coreConfig.getTimeZone()));
    }
}
