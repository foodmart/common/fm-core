package fm.common.core.config;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.Locale;

@Configuration
@DependsOn("coreConfig")
@RequiredArgsConstructor
public class LocaleConfig {

    private final CoreConfig coreConfig;

    @PostConstruct
    public void init() {
        Locale.setDefault(Locale.of(coreConfig.getLanguage(), coreConfig.getCountry()));
    }
}
