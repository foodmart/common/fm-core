package fm.common.core.config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@ConfigurationProperties(prefix = "config.core")
@PropertySource("classpath:application-core.properties")
@Configuration
@Data
public class CoreConfig {

    private String timeZone;

    private String language;

    private String country;

    private String springAppName;

    private String contextPath;

    private String gatewayUrl;

    private DefaultPageable defaultPageable;

    private Swagger swagger;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @Data
    public static class DefaultPageable {
        private int page;

        private int size;

    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @Data
    public static class Swagger {

        private String version;

        private String description;

        private Contact contact;

        private License license;


        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        @Data
        public static class Contact {
            private String name;
            private String email;
            private String url;
        }

        @Data
        @NoArgsConstructor(access = AccessLevel.PRIVATE)
        public static class License {
            private String name;
            private String url;
        }
    }

}
