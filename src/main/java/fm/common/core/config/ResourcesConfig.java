package fm.common.core.config;

import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

public class ResourcesConfig {
    @Bean
    public Set<String> availableLocals() {
        return Arrays.stream(Locale.getAvailableLocales())
                .map(Locale::getLanguage)
                .collect(Collectors.toSet());
    }
}
