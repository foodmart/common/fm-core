package fm.common.core.interceptor;

import feign.MethodMetadata;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import fm.common.core.exception.MissingParameterException;
import fm.common.core.model.annoation.Param;
import fm.common.core.model.header.CommonContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FeignInterceptor implements RequestInterceptor {

    private final CommonContext commonContext;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        Optional.ofNullable(requestTemplate.methodMetadata())
                .map(MethodMetadata::method)
                .map(method -> method.getAnnotation(fm.common.core.model.annoation.CommonParams.class))
                .ifPresent(commonParams -> {
                    setParameter(requestTemplate, commonParams.requestId(), this.commonContext.getRequestId());
                    setParameter(requestTemplate, commonParams.country(), this.commonContext.getCountry());
                    setParameter(requestTemplate, commonParams.timeZone(), this.commonContext.getTimeZone());
                });

        Optional.ofNullable(requestTemplate.methodMetadata())
                .map(MethodMetadata::method)
                .map(method -> method.getAnnotation(fm.common.core.model.annoation.Pageable.class))
                .ifPresent(pageable -> {
                    setParameter(requestTemplate, pageable.size(), String.valueOf(this.commonContext.getSize()));
                    setParameter(requestTemplate, pageable.page(), String.valueOf(this.commonContext.getPage()));
                });

    }

    private void setParameter(RequestTemplate requestTemplate, Param param, String value) {
        if (value == null) throw new MissingParameterException(param.name() + " is required");
        switch (param.type()) {
            case QUERY -> requestTemplate.query(param.name(), value);
            case HEADER -> requestTemplate.header(param.name(), value);
        }
    }
}
