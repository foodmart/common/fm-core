package fm.common.core.interceptor;

import fm.common.core.config.CoreConfig;
import fm.common.core.exception.BadRequestException;
import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.annoation.Param;
import fm.common.core.model.header.CommonContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Locale;
import java.util.Optional;

import static fm.common.core.util.TimeZoneUtil.isValidTimeZone;
import static fm.common.core.util.UUIDUtil.generateTimeBasedUUID;

@Component
@RequiredArgsConstructor
public class ParamsInterceptor implements HandlerInterceptor {

    private final CommonContext commonContext;

    private final CoreConfig coreConfig;


    @Override
    public boolean preHandle(@NonNull HttpServletRequest request,
                             @NonNull HttpServletResponse response,
                             @NonNull Object handler) {
        if (handler instanceof HandlerMethod handlerMethod) {
            Optional.ofNullable(handlerMethod.getMethodAnnotation(CommonParams.class))
                    .ifPresent(commonParams -> {
                        setTimeZone(request, commonParams.timeZone());
                        setCountry(request, commonParams.country());
                        setLanguage(request, commonParams.language());
                        setRequestId(request, commonParams.requestId());
                        commonContext.setLocale(Locale.of(commonContext.getLanguage(), commonContext.getCountry()));
                    });
            Optional.ofNullable(handlerMethod.getMethodAnnotation(Pageable.class))
                    .ifPresent(pageable -> {
                        setPage(request, pageable.page());
                        setSize(request, pageable.size());
                    });
        }
        return true;
    }

    private void setPage(HttpServletRequest request, Param param) {
        String parameter = getParameterAndValidate(request, param);
        Integer page = Optional.ofNullable(parameter)
                .map(Integer::parseInt)
                .orElse(0);
        commonContext.setPage(page);
    }

    private void setSize(HttpServletRequest request, Param param) {
        String parameter = getParameterAndValidate(request, param);
        Integer size = Optional.ofNullable(parameter)
                .map(Integer::parseInt)
                .orElse(Integer.MAX_VALUE);
        commonContext.setSize(size);
    }

    private void setRequestId(HttpServletRequest request, Param param) {
        String parameter = getParameterAndValidate(request, param);
        String requestId = Optional.ofNullable(parameter)
                .filter(id -> !id.isBlank())
                .orElse(generateTimeBasedUUID());
        commonContext.setRequestId(requestId);
    }

    private void setCountry(HttpServletRequest request, Param param) {
        String parameter = getParameterAndValidate(request, param);
        if (parameter != null) {
            commonContext.setCountry(parameter);
        } else {
            commonContext.setCountry(coreConfig.getCountry());
        }
    }

    private void setLanguage(HttpServletRequest request, Param param) {
        String parameter = getParameterAndValidate(request, param);
        if (parameter != null) {
            commonContext.setLanguage(parameter);
        } else {
            commonContext.setLanguage(coreConfig.getLanguage());
        }
    }

    private void setTimeZone(HttpServletRequest request, Param param) {
        String parameter = getParameterAndValidate(request, param);
        if (parameter != null && isValidTimeZone(parameter)) {
            commonContext.setTimeZone(parameter);
        } else {
            commonContext.setTimeZone(coreConfig.getTimeZone());
        }
    }

    private String getParameterAndValidate(HttpServletRequest request, Param param) {
        String parameter = switch (param.type()) {
            case QUERY -> request.getParameter(param.name());
            case HEADER -> request.getHeader(param.name());
        };
        if (param.required() && parameter == null) throw new BadRequestException(param.name() + " is required");
        return parameter;
    }
}
