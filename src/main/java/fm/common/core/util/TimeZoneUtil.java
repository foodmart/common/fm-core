package fm.common.core.util;

import java.time.ZoneId;

public class TimeZoneUtil {

    public static boolean isValidTimeZone(String timeZone) {
        return ZoneId.getAvailableZoneIds().contains(timeZone);
    }

}
