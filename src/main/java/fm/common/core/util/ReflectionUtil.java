package fm.common.core.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReflectionUtil {

    public static Set<Annotation> getAnnotations(Method method) {
        Set<Annotation> annotations = new HashSet<>();
        for (Annotation annotation : method.getAnnotations()) {
            collectAnnotations(annotation, annotations);
        }
        return annotations;
    }

    public static boolean isOfType(Object object, Class<?>... expectedTypes) {
        final Type[] genericInterfaces = object.getClass().getGenericInterfaces();
        if (genericInterfaces.length > 0 && genericInterfaces[0] instanceof ParameterizedType parameterizedType) {
            Type[] types = parameterizedType.getActualTypeArguments();
            if (types.length == expectedTypes.length) {
                for (int i = 0; i < types.length; i++) {
                    if (!types[i].equals(expectedTypes[i])) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }


    private static void collectAnnotations(Annotation annotation, Set<Annotation> annotations) {
        if (annotations.contains(annotation)) {
            return;
        }
        annotations.add(annotation);
        for (Annotation metaAnnotation : annotation.annotationType().getAnnotations()) {
            collectAnnotations(metaAnnotation, annotations);
        }
    }
}
