package fm.common.core.util;

import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UUIDUtil {

    public static String generateTimeBasedUUID() {
        TimeBasedGenerator uuidGenerator = Generators.timeBasedGenerator();
        UUID uuid = uuidGenerator.generate();
        return uuid.toString();
    }
}
