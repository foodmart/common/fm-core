package fm.common.core.model.annoation;

import fm.common.core.model.enums.ParamType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static fm.common.core.constant.ParamsConstants.Common.*;
import static fm.common.core.constant.SwaggerConstants.Descriptions.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface CommonParams {

    Param requestId() default @Param(required = true,
            name = REQUEST_ID,
            description = REQUEST_ID_DESCRIPTION,
            type = ParamType.HEADER);

    Param country() default @Param(name = COUNTRY,
            description = COUNTRY_DESCRIPTION,
            type = ParamType.HEADER);

    Param language() default @Param(name = LANGUAGE,
            description = LANGUAGE_DESCRIPTION,
            type = ParamType.HEADER);

    Param timeZone() default @Param(name = TIME_ZONE,
            description = TIME_ZONE_DESCRIPTION,
            type = ParamType.HEADER);
}
