package fm.common.core.model.annoation;

import fm.common.core.model.enums.ParamType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE_PARAMETER})
public @interface Param {

    boolean required() default false;

    String name() default "";

    ParamType type() default ParamType.QUERY;

    String description() default "";


}
