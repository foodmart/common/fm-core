package fm.common.core.model.annoation;

import fm.common.core.model.enums.ParamType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static fm.common.core.constant.ParamsConstants.Common.PAGE;
import static fm.common.core.constant.ParamsConstants.Common.SIZE;
import static fm.common.core.constant.SwaggerConstants.Descriptions.PAGE_DESCRIPTION;
import static fm.common.core.constant.SwaggerConstants.Descriptions.SIZE_DESCRIPTION;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Pageable {

    Param page() default @Param(required = true,
            name = PAGE,
            description = PAGE_DESCRIPTION,
            type = ParamType.QUERY);

    Param size() default @Param(required = true,
            name = SIZE,
            description = SIZE_DESCRIPTION,
            type = ParamType.QUERY);

}
