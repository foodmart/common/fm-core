package fm.common.core.model.response;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Builder
@Getter
public class ErrorResponse {

    private String exceptionName;

    private String serviceName;

    private String message;

    private String description;

    private HttpStatus status;

}
