package fm.common.core.model.response;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

public class ResponseCoreEntity<T> extends ResponseEntity<T> {
    public ResponseCoreEntity(T body, HttpStatusCode status) {
        super(body, status);
    }

    public ResponseCoreEntity(HttpStatusCode status) {
        super(status);
    }
}
