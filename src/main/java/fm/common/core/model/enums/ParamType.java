package fm.common.core.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ParamType {

    QUERY("query"), HEADER("header");

    private final String value;
}
