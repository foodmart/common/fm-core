package fm.common.core.model.header;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Locale;

@Component
@RequestScope
@Data
public class CommonContext {

    private String timeZone;

    private String requestId;

    private Locale locale;

    private String language;

    private String country;

    private int page;

    private int size;

}
