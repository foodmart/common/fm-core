package fm.common.core.controller;

import fm.common.core.model.response.ResponseCoreEntity;
import org.springframework.http.HttpStatus;

public class BaseController {

    public <T> ResponseCoreEntity<T> response(T t) {
        return new ResponseCoreEntity<>(t, HttpStatus.OK);
    }

    public <T> ResponseCoreEntity<T> response(T t, HttpStatus httpStatus) {
        return new ResponseCoreEntity<>(t, httpStatus);
    }

    public <T> ResponseCoreEntity<T> response(HttpStatus httpStatus) {
        return new ResponseCoreEntity<>(httpStatus);
    }
}
