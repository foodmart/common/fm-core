package fm.common.core.advice;

import fm.common.core.config.CoreConfig;
import fm.common.core.exception.GenericException;
import fm.common.core.model.response.ErrorResponse;
import fm.common.core.service.ResourceBundlesService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@AllArgsConstructor
public class ExceptionsAdvice {

    private final CoreConfig coreConfig;

    private final ResourceBundlesService resourceBundlesService;

    @ExceptionHandler(GenericException.class)
    public <T extends GenericException> ResponseEntity<ErrorResponse> handleGenericExceptions(T t) {
        String exceptionMessage;
        try {
            exceptionMessage = resourceBundlesService.getExceptionMessage(GenericException.class, t);
        } catch (Exception e) {
            return handleExceptions(t);
        }
        return ResponseEntity.status(t.getHttpStatus())
                .body(ErrorResponse.builder()
                        .exceptionName(t.getClass().getSimpleName())
                        .message(t.getMessage())
                        .status(t.getHttpStatus())
                        .description(exceptionMessage)
                        .serviceName(coreConfig.getSpringAppName())
                        .build());
    }

    @ExceptionHandler(Exception.class)
    public <T extends Exception> ResponseEntity<ErrorResponse> handleExceptions(T t) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorResponse.builder()
                        .exceptionName(t.getClass().getSimpleName())
                        .message(t.getMessage())
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .serviceName(coreConfig.getSpringAppName())
                        .build());
    }


}
