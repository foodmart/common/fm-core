package fm.common.core.advice;

import fm.common.core.model.header.CommonContext;
import fm.common.core.model.response.ResponseBodyEntity;
import fm.common.core.model.response.ResponseCoreEntity;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.Method;

@ControllerAdvice(basePackages = "fm")
@RequiredArgsConstructor
public class ResponseCoreAdvice implements ResponseBodyAdvice<Object> {

    private final CommonContext commonContext;

    @Override
    public boolean supports(@NonNull MethodParameter returnType, @NonNull Class<? extends HttpMessageConverter<?>> converterType) {
        final Method method = returnType.getMethod();
        if (method != null) {
            return method.getReturnType().equals(ResponseCoreEntity.class);
        }
        return false;
    }

    @Override
    public Object beforeBodyWrite(Object body,
                                  @NonNull MethodParameter returnType,
                                  @NonNull MediaType selectedContentType,
                                  @NonNull Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  @NonNull ServerHttpRequest request,
                                  @NonNull ServerHttpResponse response) {

        return ResponseBodyEntity.builder()
                .data(body)
                .build();
    }
}
