package fm.common.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BadRequestException extends GenericException {

    private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }
}
