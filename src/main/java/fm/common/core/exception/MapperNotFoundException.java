package fm.common.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class MapperNotFoundException extends GenericException {

    private final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public MapperNotFoundException() {
    }

    public MapperNotFoundException(String message) {
        super(message);
    }
}
