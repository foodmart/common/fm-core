package fm.common.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ManyMappersFoundException extends GenericException {

    private final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public ManyMappersFoundException() {
    }

    public ManyMappersFoundException(String message) {
        super(message);
    }
}
