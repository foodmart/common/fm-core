package fm.common.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ResourcesException extends GenericException {

    private final HttpStatus httpStatus = HttpStatus.BAD_REQUEST;

    public ResourcesException() {
    }

    public ResourcesException(String message) {
        super(message);
    }
}
