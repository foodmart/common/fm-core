package fm.common.core.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class GenericException extends RuntimeException {

    private final HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

    public GenericException() {
    }

    public GenericException(String message) {
        super(message);
    }
}
